﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserService_Module
{
    public static class ServiceConfiguration
    {
        private static bool startAsService = true;

        public static bool StartAsService
        {
            get { return startAsService; }
            set { startAsService = value; }
        }
    }
}
