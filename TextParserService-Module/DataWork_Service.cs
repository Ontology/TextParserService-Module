﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace TextParserService_Module
{
    public class DataWork_Service
    {
        private clsLocalConfig _localConfig;
        public List<LoggingItem> LoggingItems { get; set; }

        public clsOntologyItem OItem_TextParser { get; set; }

        private OntologyModDBConnector _dbLevel_Codes;
        private OntologyModDBConnector _dbLevel_Types;

        public clsOntologyItem GetData()
        {
            LoggingItems = new List<LoggingItem>();

            var result = GetSubData_001_LoggingItems();
            if (result.GUID == _localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_002_LoggingTypes();
                if (result.GUID == _localConfig.Globals.LState_Success.GUID)
                {
                    LoggingItems = (from item in _dbLevel_Codes.ObjectRels
                                    join itemType in _dbLevel_Types.ObjectRels on item.ID_Object equals itemType.ID_Object
                                    select new LoggingItem
                                    {
                                        IdLoggingItem = item.ID_Object,
                                        NameLoggingItem = item.Name_Object,
                                        LoggingCode = GetLoggingCode(item),
                                        IdLoggingType = itemType.ID_Other,
                                        NameLoggingType = itemType.Name_Other

                                    }).ToList();    
                }
                
            }
            return result;
        }

        private int GetLoggingCode(clsObjectRel loggingItem)
        {
            if (loggingItem != null)
            {
                var result = 0;
                if (int.TryParse(loggingItem.Name_Other, out result))
                {
                    return result;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
            
        }

        private clsOntologyItem GetSubData_001_LoggingItems()
        {
            var searchLoggingCodes = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = _localConfig.OItem_object_no_database_service.GUID,
                    ID_RelationType = _localConfig.OItem_relationtype_defines.GUID,
                    ID_Parent_Other = _localConfig.OItem_class_logging_codes.GUID
                },
                new clsObjectRel
                {
                    ID_Object = _localConfig.OItem_object_wrong_count_of_arguments.GUID,
                    ID_RelationType = _localConfig.OItem_relationtype_defines.GUID,
                    ID_Parent_Other = _localConfig.OItem_class_logging_codes.GUID
                },
                new clsObjectRel
                {
                    ID_Object = _localConfig.OItem_object_configuration_error.GUID,
                    ID_RelationType = _localConfig.OItem_relationtype_defines.GUID,
                    ID_Parent_Other = _localConfig.OItem_class_logging_codes.GUID
                }
            };

            var result = _dbLevel_Codes.GetDataObjectRel(searchLoggingCodes, doIds: false);

            return result;
        }

        public clsOntologyItem GetSubData_002_LoggingTypes()
        {
            var searchType = _dbLevel_Codes.ObjectRels.Select(logItem => new clsObjectRel
            {
                
                ID_Object = logItem.ID_Object,
                ID_RelationType = _localConfig.OItem_relationtype_is_in_state.GUID,
                ID_Parent_Other = _localConfig.OItem_class_logstate.GUID
                
            }).ToList();

            var result = _dbLevel_Types.GetDataObjectRel(searchType);

            return result;
        }

        public DataWork_Service(clsLocalConfig localConfig)
        {
            _localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            _dbLevel_Codes = new OntologyModDBConnector(_localConfig.Globals);
            _dbLevel_Types = new OntologyModDBConnector(_localConfig.Globals);
        }
    }
}
