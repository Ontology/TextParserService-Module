﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TextParserService_Module
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new TextParserService() 
            };
            if (ServiceConfiguration.StartAsService)
            {
                ServiceBase.Run(ServicesToRun);    
            }
            
        }
    }
}
