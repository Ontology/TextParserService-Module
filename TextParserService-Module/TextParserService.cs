﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Filesystem_Module;
using OntologyAppDBConnector;
using TextParser;
using PortListenerForText_Module;

namespace TextParserService_Module
{
    [Flags]
    public enum ErrorCodes
    {
        ConfigError = 14000,
        NoDatabaseService = 15000
    }

    [Flags]
    public enum ParserType
    {
        PortListener = 0,
        FileListener = 1
    }
    public partial class TextParserService : ServiceBase
    {
        private clsLocalConfig localConfig;
        private TextParser.clsLocalConfig localConfig_TextParser;
        private Filesystem_Module.clsLocalConfig localConfig_FileSystemModule;
        private PortListenerForText_Module.clsLocalConfig localConfig_PortListener;
        private clsDataWork_TextParser dataWorkTextParser;
        private clsDataWork_FieldParser dataWorkFieldParser;
        private clsDataWork_FileResources dataWorkFileResources;
        private clsDataWork_FileResource_Path dataWorkFileResourcePath;
        private clsDataWork_PortListener dataWorkPortListener;
        private DataWork_Service dataWorkService;
        private frmPortListenerForText frmPortListenerForText;
        private List<clsFile> fileList;
        private clsPortConfig portConfiguration;
        private clsFieldParserOfTextParser fieldParserOfTextParser;

        private ParserType parserType;
        private int lineCounter;
        private int totalLines;

        private EventLog eventLog;

        public TextParserService()
        {
            InitializeComponent();
            this.AutoLog = false;
            if (!EventLog.SourceExists("TextParserService-Module"))
            {
                EventLog.CreateEventSource("TextParserService-Module", "TextParserService-ModuleLog");
            }

            eventLog_Parser.Source = "TextParserService-Module";
            eventLog_Parser.Log = "TextParserService-ModuleLog";
            try
            {
                localConfig = new clsLocalConfig(new Globals());
                localConfig_TextParser = new TextParser.clsLocalConfig(localConfig.Globals);
                Initialize();
            }
            catch (Exception)
            {
                eventLog_Parser.WriteEntry("NoDatabaseService",EventLogEntryType.Error,(int)ErrorCodes.NoDatabaseService);
                Stop();
                this.ExitCode = (int)ErrorCodes.NoDatabaseService;
                throw;
            }
            

            
        }

        private void Initialize()
        {
            dataWorkService = new DataWork_Service(localConfig);
            dataWorkTextParser = new clsDataWork_TextParser(localConfig_TextParser);
            dataWorkFieldParser = new clsDataWork_FieldParser(localConfig_TextParser);
            localConfig_FileSystemModule = new Filesystem_Module.clsLocalConfig(localConfig.Globals);
            localConfig_PortListener = new PortListenerForText_Module.clsLocalConfig(localConfig.Globals);
            dataWorkFileResources = new clsDataWork_FileResources(localConfig_FileSystemModule);
            dataWorkFileResourcePath = new clsDataWork_FileResource_Path(localConfig_FileSystemModule);
            dataWorkPortListener = new clsDataWork_PortListener(localConfig_PortListener);
            
            
            var result = dataWorkService.GetData();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                eventLog_Parser.WriteEntry("Configuration-Error", EventLogEntryType.Error,(int)ErrorCodes.ConfigError);
                this.ExitCode = 14000;
                throw new Exception("Configuration-Error");
            }

            
        }

        public void Start(List<string> args)
        {
            eventLog_Parser.WriteEntry("Starting...");

            ArgumentParsing.ParseArguments(localConfig.Globals,args);

            if (ArgumentParsing.Items != null && ArgumentParsing.Items.Count == 1)
            {
                var item = ArgumentParsing.Items.First();

                if (item.GUID_Parent == localConfig.OItem_class_textparser.GUID)
                {
                    if (ArgumentParsing.FunctionList != null && ArgumentParsing.FunctionList.Count == 1)
                    {
                        if (ArgumentParsing.FunctionList.Any(
                            func => func.GUIDFunction == localConfig.OItem_object_filelistener.GUID))
                        {
                            parserType = ParserType.FileListener;
                        }
                        else if (ArgumentParsing.FunctionList.Any(
                            func => func.GUIDFunction == localConfig.OItem_object_portlistener.GUID))
                        {
                            parserType = ParserType.PortListener;
                        }
                        else
                        {
                            var errorItem =
                                dataWorkService.LoggingItems.FirstOrDefault(
                                logItem => logItem.IdLoggingItem == localConfig.OItem_object_parsing_function_must_be_provided.GUID);

                            eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error,errorItem.LoggingCode);
                            this.ExitCode = errorItem.LoggingCode;
                            Stop();
                            throw new Exception(errorItem.NameLoggingItem);
                        }
                        dataWorkService.OItem_TextParser = ArgumentParsing.Items.First().Clone();
                        GetDataTextParser();
                    }
                    else
                    {
                        var errorItem =
                            dataWorkService.LoggingItems.FirstOrDefault(
                            logItem => logItem.IdLoggingItem == localConfig.OItem_object_parsing_function_must_be_provided.GUID);

                        eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
                        this.ExitCode = errorItem.LoggingCode;
                        Stop();
                        throw new Exception(errorItem.NameLoggingItem);
                    }
                }
                else
                {
                    var errorItem =
                        dataWorkService.LoggingItems.FirstOrDefault(
                        logItem => logItem.IdLoggingItem == localConfig.OItem_object_parameter_must_be_a_textparser.GUID);

                    eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
                    this.ExitCode = errorItem.LoggingCode;
                    Stop();
                    throw new Exception(errorItem.NameLoggingItem);
                }
            }
            else
            {
                var errorItem =
                    dataWorkService.LoggingItems.FirstOrDefault(
                        logItem => logItem.IdLoggingItem == localConfig.OItem_object_wrong_count_of_arguments.GUID);

                eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
                this.ExitCode = errorItem.LoggingCode;
                throw new Exception(errorItem.NameLoggingItem);
            }
            eventLog_Parser.WriteEntry("Started");
        }

        private void GetDataTextParser()
        {
            dataWorkTextParser.GetData_TextParser(dataWorkService.OItem_TextParser);
            if (dataWorkTextParser.OItem_Result_TextParser.GUID == localConfig.Globals.LState_Success.GUID)
            {
                dataWorkTextParser.CreateRefItems(dataWorkService.OItem_TextParser);
                if (dataWorkTextParser.OItem_FieldExtractorParser != null)
                {
                    dataWorkTextParser.GetData_IndexData(dataWorkTextParser.OItem_Index);
                    while (dataWorkTextParser.OItem_Result_Index.GUID == localConfig.Globals.LState_Nothing.GUID) { }

                    if (dataWorkTextParser.OItem_Result_Index.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var result = dataWorkFieldParser.GetData_FieldsOfFieldParser();

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var fieldList =
                                dataWorkFieldParser.FieldList.Where(
                                    fld => fld.ID_FieldParser == dataWorkTextParser.OItem_FieldExtractorParser.GUID)
                                    .OrderBy(fld => fld.OrderId)
                                    .ToList();
                            if (parserType == ParserType.FileListener && dataWorkTextParser.OItem_FileResource != null)
                            {
                                var resourceType =
                                    dataWorkFileResources.GetResourceType(dataWorkTextParser.OItem_FileResource);

                                if (resourceType.GUID == dataWorkFileResources.OItem_Class_Path.GUID)
                                {
                                    dataWorkFileResourcePath.GetData_Attributes(dataWorkTextParser.OItem_FileResource);
                                    if (dataWorkFileResourcePath.OItem_Result_Attributes.GUID ==
                                        localConfig.Globals.LState_Success.GUID)
                                    {
                                        dataWorkFileResourcePath.GetData_Relations(dataWorkTextParser.OItem_FileResource);
                                        if (dataWorkFileResourcePath.OItem_Result_Relations.GUID ==
                                            localConfig.Globals.LState_Success.GUID)
                                        {
                                            dataWorkFileResourcePath.GetFiles();
                                            if (dataWorkFileResourcePath.OItem_Result_FileResult.GUID ==
                                                localConfig.Globals.LState_Success.GUID)
                                            {
                                                fileList = dataWorkFileResourcePath.FileList;
                                                var folderList =
                                                    fileList.Select(file => Path.GetDirectoryName(file.FileName))
                                                        .GroupBy(folder => folder)
                                                        .Select(folder => folder.Key)
                                                        .ToList();

                                                if (folderList.Count == 1)
                                                {
                                                    fileSystemWatcher_Parser.Path = folderList.First();
                                                    fileSystemWatcher_Parser.EnableRaisingEvents = true;
                                                }
                                                else
                                                {
                                                    var errorItem =
                                                        dataWorkService.LoggingItems.FirstOrDefault(
                                                        logItem => logItem.IdLoggingItem == localConfig.OItem_object_only_one_folder_for_textparsing_service.GUID);

                                                    eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
                                                }

                                                

                                            }
                                            else
                                            {
                                                
                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                else
                                {
                                    
                                }
                            }
                            else if (parserType == ParserType.PortListener)
                            {
                                frmPortListenerForText = new frmPortListenerForText(localConfig_PortListener, dataWorkService.OItem_TextParser);
                                frmPortListenerForText.textFromStream += frmPortListenerForText_textFromStream;
                                frmPortListenerForText.clientClosed += frmPortListenerForText_clientClosed;
                                portConfiguration = frmPortListenerForText.PortConfigurationResource;
                                fieldParserOfTextParser = new clsFieldParserOfTextParser(localConfig_TextParser, fieldList, dataWorkService.OItem_TextParser,dataWorkTextParser.OITem_Type);
                                fieldParserOfTextParser.OList_Seperator = dataWorkTextParser.OList_LineSeperator;
                                fieldParserOfTextParser.InitializeFields();
                                totalLines = 0;
                                lineCounter = 0;
                                frmPortListenerForText.Initialize();

                                
                            }
                            else
                            {

                            }

                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                    
                }
                else
                {
                    
                }
            }
            else
            {
                
            }
        }

        void frmPortListenerForText_clientClosed()
        {
            var result = fieldParserOfTextParser.SaveDocuments();
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                var errorItem =
                   dataWorkService.LoggingItems.FirstOrDefault(
                       logItem => logItem.IdLoggingItem == localConfig.OItem_object_parsing_error.GUID);

                eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
            }
        }

        void frmPortListenerForText_textFromStream(string line)
        {
            long maxCount = 0;
            if (portConfiguration != null)
            {
                if (portConfiguration.TypeOfSplit == SplitType.Count)
                {
                    maxCount = portConfiguration.LineCountBeforeSaving;
                }
                else
                {
                    maxCount = fieldParserOfTextParser.PageCount;
                        
                }
            }   

            var result = fieldParserOfTextParser.ParseText(line, totalLines, maxCount);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                lineCounter++;
                totalLines++;

                if (portConfiguration != null)
                {
                    if (portConfiguration.TypeOfSplit == SplitType.Count)
                    {
                        if (lineCounter >= portConfiguration.LineCountBeforeSaving)
                        {
                            result = fieldParserOfTextParser.SaveDocuments();
                            lineCounter = 0;
                        }
                    }
                    else if (portConfiguration.TypeOfSplit == SplitType.Seperator)
                    {
                        if (line.ToLower() == portConfiguration.NameSeperator)
                        {
                            result = fieldParserOfTextParser.SaveDocuments();
                            lineCounter = 0;
                        }
                    }
                    else
                    {
                        if (lineCounter >= fieldParserOfTextParser.PageCount)
                        {
                            result = fieldParserOfTextParser.SaveDocuments();
                            lineCounter = 0;
                        }
                    }
                }    
            }

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                var errorItem =
                   dataWorkService.LoggingItems.FirstOrDefault(
                       logItem => logItem.IdLoggingItem == localConfig.OItem_object_parsing_error.GUID);

                eventLog_Parser.WriteEntry(errorItem.NameLoggingItem, EventLogEntryType.Error, errorItem.LoggingCode);
            }
            
        }

        protected override void OnStart(string[] args)
        {
            
            Start(args.ToList());
        }

        protected override void OnStop()
        {
            eventLog_Parser.WriteEntry("Stopping Service");
        }

        private void fileSystemWatcher_Parser_Changed(object sender, FileSystemEventArgs e)
        {
            eventLog_Parser.WriteEntry("Getting filechanges is not implemented",EventLogEntryType.Warning);
        }
    }
}
