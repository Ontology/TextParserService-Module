﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using ElasticSearchLogging_Module;

namespace TextParserService_Module
{
    public class clsLocalConfig
    {
        private const string cstrID_Ontology = "bc1080757835462faa1b46919a7b409e";
        private ImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        // Classes
        public clsOntologyItem OItem_class_textparser { get; set; }
        public clsOntologyItem OItem_class_logging_codes { get; set; }
        public clsOntologyItem OItem_class_logstate { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_defines { get; set; }
        public clsOntologyItem OItem_relationtype_is_in_state { get; set; }

        // Objects
        public clsOntologyItem OItem_object_textparserservice_module { get; set; }
        public clsOntologyItem OItem_object_textparserservice { get; set; }
        public clsOntologyItem OItem_object_portlistener { get; set; }
        public clsOntologyItem OItem_object_filelistener { get; set; }
        public clsOntologyItem OItem_object_wrong_count_of_arguments { get; set; }
        public clsOntologyItem OItem_object_no_database_service { get; set; }
        public clsOntologyItem OItem_object_configuration_error { get; set; }
        public clsOntologyItem OItem_object_parameter_must_be_a_textparser { get; set; }
        public clsOntologyItem OItem_object_parsing_function_must_be_provided { get; set; }
        public clsOntologyItem OItem_object_only_one_folder_for_textparsing_service { get; set; }
        public clsOntologyItem OItem_object_parsing_error { get; set; }

public clsLogging Logging { get; set; }
	
private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology, 
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID, 
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                ID_RelationType = Globals.RelationType_belongingAttribute.GUID}).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                    ID_RelationType = Globals.RelationType_belongingClass.GUID}));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel {ID_Object = oi.ID_Other,
                                                                                                                                    ID_RelationType = Globals.RelationType_belongingObject.GUID}));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds:false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }   
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }
  
	public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }
  
	private void set_DBConnection()
        {
		    objDBLevel_Config1 = new OntologyModDBConnector(Globals);
		    objDBLevel_Config2 = new OntologyModDBConnector(Globals);
			objImport = new ImportWorker(Globals);
        }

        private void InitializeLogging()
        {
            Logging = new clsLogging(Globals);
            var result = Logging.Initialize_Logging(OItem_object_textparserservice);
            if (result.GUID == Globals.LState_Error.GUID)
            {
                throw new Exception("Config-Error");
            }

        }

        private bool AutoImportItems
        {
            get
            {
                bool result;
                string autoImportItems = AppConfiguration.GetValue("AutoImportItems");

                if (!string.IsNullOrEmpty(autoImportItems) && bool.TryParse(autoImportItems, out result))
                {
                    return result;
                }
                else
                {
                    return false;
                }
            }
        }
  
	private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
                InitializeLogging();
            }
            catch(Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[]) objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1) 
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                if (AutoImportItems)
                {
                    var objOItem_Result = objImport.ImportTemplates(objAssembly);
                    if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                    {
                        get_Data_DevelopmentConfig();
                        get_Config_AttributeTypes();
                        get_Config_RelationTypes();
                        get_Config_Classes();
                        get_Config_Objects();
                    }
                    else
                    {
                        throw new Exception("Config not importable");
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }
  
	private void get_Config_AttributeTypes()
        {
		
	}
  
	private void get_Config_RelationTypes()
        {
            var objOList_relationtype_is_in_state = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_is_in_state".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

            if (objOList_relationtype_is_in_state.Any())
            {
                OItem_relationtype_is_in_state = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_in_state.First().ID_Other,
                    Name = objOList_relationtype_is_in_state.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_in_state.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_defines = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "relationtype_defines".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                 select objRef).ToList();

            if (objOList_relationtype_defines.Any())
            {
                OItem_relationtype_defines = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_defines.First().ID_Other,
                    Name = objOList_relationtype_defines.First().Name_Other,
                    GUID_Parent = objOList_relationtype_defines.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }
	}
  
	private void get_Config_Objects()
        {
            var objOList_object_parsing_error = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "object_parsing_error".ToLower() && objRef.Ontology == Globals.Type_Object
                                                 select objRef).ToList();

            if (objOList_object_parsing_error.Any())
            {
                OItem_object_parsing_error = new clsOntologyItem()
                {
                    GUID = objOList_object_parsing_error.First().ID_Other,
                    Name = objOList_object_parsing_error.First().Name_Other,
                    GUID_Parent = objOList_object_parsing_error.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_only_one_folder_for_textparsing_service = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                           where objOItem.ID_Object == cstrID_Ontology
                                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                           where objRef.Name_Object.ToLower() == "object_only_one_folder_for_textparsing_service".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                           select objRef).ToList();

            if (objOList_object_only_one_folder_for_textparsing_service.Any())
            {
                OItem_object_only_one_folder_for_textparsing_service = new clsOntologyItem()
                {
                    GUID = objOList_object_only_one_folder_for_textparsing_service.First().ID_Other,
                    Name = objOList_object_only_one_folder_for_textparsing_service.First().Name_Other,
                    GUID_Parent = objOList_object_only_one_folder_for_textparsing_service.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_parsing_function_must_be_provided = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                     where objOItem.ID_Object == cstrID_Ontology
                                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                     where objRef.Name_Object.ToLower() == "object_parsing_function_must_be_provided".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                     select objRef).ToList();

            if (objOList_object_parsing_function_must_be_provided.Any())
            {
                OItem_object_parsing_function_must_be_provided = new clsOntologyItem()
                {
                    GUID = objOList_object_parsing_function_must_be_provided.First().ID_Other,
                    Name = objOList_object_parsing_function_must_be_provided.First().Name_Other,
                    GUID_Parent = objOList_object_parsing_function_must_be_provided.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_parameter_must_be_a_textparser = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                  where objOItem.ID_Object == cstrID_Ontology
                                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                  where objRef.Name_Object.ToLower() == "object_parameter_must_be_a_textparser".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                  select objRef).ToList();

            if (objOList_object_parameter_must_be_a_textparser.Any())
            {
                OItem_object_parameter_must_be_a_textparser = new clsOntologyItem()
                {
                    GUID = objOList_object_parameter_must_be_a_textparser.First().ID_Other,
                    Name = objOList_object_parameter_must_be_a_textparser.First().Name_Other,
                    GUID_Parent = objOList_object_parameter_must_be_a_textparser.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_configuration_error = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "object_configuration_error".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

            if (objOList_object_configuration_error.Any())
            {
                OItem_object_configuration_error = new clsOntologyItem()
                {
                    GUID = objOList_object_configuration_error.First().ID_Other,
                    Name = objOList_object_configuration_error.First().Name_Other,
                    GUID_Parent = objOList_object_configuration_error.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }
            var objOList_object_wrong_count_of_arguments = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "object_wrong_count_of_arguments".ToLower() && objRef.Ontology == Globals.Type_Object
                                                            select objRef).ToList();

            if (objOList_object_wrong_count_of_arguments.Any())
            {
                OItem_object_wrong_count_of_arguments = new clsOntologyItem()
                {
                    GUID = objOList_object_wrong_count_of_arguments.First().ID_Other,
                    Name = objOList_object_wrong_count_of_arguments.First().Name_Other,
                    GUID_Parent = objOList_object_wrong_count_of_arguments.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_no_database_service = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "object_no_database_service".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

            if (objOList_object_no_database_service.Any())
            {
                OItem_object_no_database_service = new clsOntologyItem()
                {
                    GUID = objOList_object_no_database_service.First().ID_Other,
                    Name = objOList_object_no_database_service.First().Name_Other,
                    GUID_Parent = objOList_object_no_database_service.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

		var objOList_object_filelistener = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "object_filelistener".ToLower() && objRef.Ontology == Globals.Type_Object
                                           select objRef).ToList();

            if (objOList_object_filelistener.Any())
            {
                OItem_object_filelistener = new clsOntologyItem()
                {
                    GUID = objOList_object_filelistener.First().ID_Other,
                    Name = objOList_object_filelistener.First().Name_Other,
                    GUID_Parent = objOList_object_filelistener.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

var objOList_object_portlistener = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "object_portlistener".ToLower() && objRef.Ontology == Globals.Type_Object
                                           select objRef).ToList();

            if (objOList_object_portlistener.Any())
            {
                OItem_object_portlistener = new clsOntologyItem()
                {
                    GUID = objOList_object_portlistener.First().ID_Other,
                    Name = objOList_object_portlistener.First().Name_Other,
                    GUID_Parent = objOList_object_portlistener.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_textparserservice = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "object_textparserservice".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_object_textparserservice.Any())
            {
                OItem_object_textparserservice = new clsOntologyItem()
                {
                    GUID = objOList_object_textparserservice.First().ID_Other,
                    Name = objOList_object_textparserservice.First().Name_Other,
                    GUID_Parent = objOList_object_textparserservice.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_textparserservice_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "object_textparserservice_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                            select objRef).ToList();

            if (objOList_object_textparserservice_module.Any())
            {
                OItem_object_textparserservice_module = new clsOntologyItem()
                {
                    GUID = objOList_object_textparserservice_module.First().ID_Other,
                    Name = objOList_object_textparserservice_module.First().Name_Other,
                    GUID_Parent = objOList_object_textparserservice_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }
	}
  
	private void get_Config_Classes()
        {
            var objOList_class_logstate = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_logstate".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_logstate.Any())
            {
                OItem_class_logstate = new clsOntologyItem()
                {
                    GUID = objOList_class_logstate.First().ID_Other,
                    Name = objOList_class_logstate.First().Name_Other,
                    GUID_Parent = objOList_class_logstate.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_logging_codes = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "class_logging_codes".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

            if (objOList_class_logging_codes.Any())
            {
                OItem_class_logging_codes = new clsOntologyItem()
                {
                    GUID = objOList_class_logging_codes.First().ID_Other,
                    Name = objOList_class_logging_codes.First().Name_Other,
                    GUID_Parent = objOList_class_logging_codes.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_textparser = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_textparser".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_class_textparser.Any())
            {
                OItem_class_textparser = new clsOntologyItem()
                {
                    GUID = objOList_class_textparser.First().ID_Other,
                    Name = objOList_class_textparser.First().Name_Other,
                    GUID_Parent = objOList_class_textparser.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }
	}
    }

}