﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextParserService_Module
{
    public class LoggingItem
    {
        public string IdLoggingItem { get; set; }
        public string NameLoggingItem { get; set; }
        public int LoggingCode { get; set; }
        public string IdLoggingType { get; set; }
        public string NameLoggingType { get; set; }
    }
}
