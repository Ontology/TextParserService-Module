﻿namespace TextParserService_Module
{
    partial class TextParserService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eventLog_Parser = new System.Diagnostics.EventLog();
            this.fileSystemWatcher_Parser = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog_Parser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher_Parser)).BeginInit();
            // 
            // fileSystemWatcher_Parser
            // 
            this.fileSystemWatcher_Parser.EnableRaisingEvents = true;
            this.fileSystemWatcher_Parser.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher_Parser_Changed);
            // 
            // TextParserService
            // 
            this.ServiceName = "ServiceSource";
            ((System.ComponentModel.ISupportInitialize)(this.eventLog_Parser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher_Parser)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog eventLog_Parser;
        private System.IO.FileSystemWatcher fileSystemWatcher_Parser;
    }
}
